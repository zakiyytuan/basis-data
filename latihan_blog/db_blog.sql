-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Sep 2020 pada 06.25
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_blog`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `blog`
--

INSERT INTO `blog` (`id`, `title`, `content`, `created_at`, `updated_at`, `deleted_at`, `category`) VALUES
(1, '7 Makanan Tradisional Khas Jawa yang Cocok Disajikan saat HUT RI ke-75', 'Jakarta - Makanan tradisional khas Jawa pilihannya sangat beragam. Semua makanan ini memiliki makna spesial sehingga jadi sajian populer saat perayaan-perayaan tertentu, termasuk pada Hari Kemerdekaan HUT RI yang ke-75 ini.', NULL, NULL, NULL, 'HUT RI ke-75'),
(2, 'test', 'test', NULL, NULL, NULL, 'test'),
(3, 'Miss', 'Aut totam sed accusantium aut dicta tempore ut quia. Id beatae aut ut provident et autem. Aliquid velit consequatur aliquam hic a sunt.', '2020-09-16 18:26:52', '2020-09-16 18:26:52', NULL, 'Bridge Tender OR Lock Tender'),
(4, 'Dr.', 'Sit in aperiam exercitationem est atque natus voluptate. Rem sed dolorem incidunt ab sed quos reprehenderit. Laudantium dolorem voluptatibus quia iste iste in provident. Ipsam quos assumenda qui nisi voluptatem ab cumque inventore. Expedita et consequatur enim velit.', '2020-09-16 18:26:52', '2020-09-16 18:26:52', NULL, 'Artillery Officer'),
(5, 'Mr.', 'Deserunt aut sit ea tenetur alias sed earum ea. Beatae culpa perferendis voluptatem ex vero. Enim et voluptatem molestiae corporis. Unde sed odit eaque veritatis.', '2020-09-16 18:26:52', '2020-09-16 18:26:52', NULL, 'Dragline Operator'),
(6, 'Prof.', 'Molestias sunt aut quis qui deserunt sapiente consequuntur voluptates. Nihil molestiae voluptatum quia delectus doloremque adipisci tenetur. Maxime quia eaque est quasi. Ipsam aut possimus molestias qui consectetur blanditiis.', '2020-09-16 18:26:52', '2020-09-16 18:26:52', NULL, 'Sheet Metal Worker'),
(7, 'Prof.', 'Est velit voluptatem itaque est. Ut culpa eos est eaque magnam qui. Et natus dignissimos culpa et cupiditate.', '2020-09-16 18:26:52', '2020-09-16 18:26:52', NULL, 'Electrical Sales Representative'),
(8, 'Mr.', 'Facere quia qui architecto. Earum distinctio sapiente numquam labore. Sunt ut necessitatibus non id non fugit eaque. Aliquam porro aut inventore vero et officia.', '2020-09-16 18:26:52', '2020-09-16 18:26:52', NULL, 'Glass Blower'),
(9, 'Mr.', 'Ea consectetur dolor labore dignissimos placeat officiis eius. Accusantium molestias repellat minus fuga recusandae atque aut. Eius commodi recusandae dolorum velit quam quia soluta. Aut aut voluptatem aut aspernatur asperiores facilis maxime.', '2020-09-16 18:26:52', '2020-09-16 18:26:52', NULL, 'Chef'),
(10, 'Prof.', 'Velit eum quod ut mollitia perspiciatis. Velit deserunt fugit distinctio et eius. Optio recusandae sapiente alias harum magni voluptatibus. Excepturi laboriosam ratione pariatur corrupti aspernatur ducimus.', '2020-09-16 18:26:52', '2020-09-16 18:26:52', NULL, 'Home Appliance Repairer'),
(11, 'Dr.', 'Cumque voluptatem delectus laboriosam repellat. Nam sint id aliquam velit blanditiis aut alias tempore. Voluptatem perferendis corrupti est eos. Dolorum debitis distinctio dolores eaque eligendi laborum est. Sint voluptatem et illum harum architecto debitis.', '2020-09-16 18:26:52', '2020-09-16 18:26:52', NULL, 'Geological Sample Test Technician'),
(12, 'Dr.', 'Quis nam aut ratione doloremque et exercitationem numquam natus. Qui cumque quia cum impedit magni quia impedit. Tempora est sint iusto non sequi vero.', '2020-09-16 18:26:52', '2020-09-16 18:26:52', NULL, 'Cement Mason and Concrete Finisher'),
(13, 'Prof.', 'Odio corrupti debitis suscipit. Quam quas architecto doloremque nisi. Nihil molestiae id quos voluptatum ut nobis. Eum repellat doloribus esse qui voluptas minima.', '2020-09-16 18:26:52', '2020-09-16 18:26:52', NULL, 'Home Entertainment Equipment Installer'),
(14, 'Miss', 'Minima et in quasi. Facilis laboriosam sit adipisci quaerat et et. Perspiciatis sint beatae repellat et. Doloremque est voluptas rem possimus suscipit.', '2020-09-16 18:26:52', '2020-09-16 18:26:52', NULL, 'Central Office'),
(15, 'Mr.', 'Voluptatem porro cupiditate in sunt nisi. Culpa autem dolorem doloremque. Minima quia sunt voluptatibus. Id quia corrupti aliquid eligendi vel soluta sit.', '2020-09-16 18:26:52', '2020-09-16 18:26:52', NULL, 'Supervisor Fire Fighting Worker'),
(16, 'Dr.', 'Nihil doloremque quia quasi dolores ipsum quas. Ut ut laborum eaque aut vitae ut. Repellat aspernatur suscipit voluptatem.', '2020-09-16 18:26:52', '2020-09-16 18:26:52', NULL, 'Order Filler'),
(17, 'Mrs.', 'Rerum sunt illum non commodi adipisci quas. Maiores veritatis vero optio ipsa.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Transportation Equipment Maintenance'),
(18, 'Mrs.', 'Voluptas est ut mollitia earum. Praesentium quia eos vero laudantium excepturi maiores saepe molestiae. Ea soluta nisi laudantium eius dolorem consequatur.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Ophthalmic Laboratory Technician'),
(19, 'Prof.', 'Quae reiciendis autem exercitationem necessitatibus. Maxime asperiores natus exercitationem omnis quae mollitia aperiam. Accusamus accusamus modi qui esse quis. Et quia ratione et necessitatibus.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Forest and Conservation Technician'),
(20, 'Dr.', 'Aut voluptatem assumenda saepe eum quis et ut. Odit iusto eligendi esse officiis consequatur exercitationem. Aliquid qui libero a inventore. Vel officiis provident id in.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Streetcar Operator'),
(21, 'Dr.', 'Nisi neque eos vitae. Qui sit soluta nulla voluptatem quae vel reiciendis. Non numquam nostrum est laboriosam. Adipisci sit quisquam fugit dolore sed dolore.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Continuous Mining Machine Operator'),
(22, 'Mr.', 'Architecto eum totam pariatur minima cupiditate quasi. Sit voluptas eos impedit rerum. Debitis et dicta odio sunt velit odio. Rerum necessitatibus eius et eum sed aut.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Motor Vehicle Inspector'),
(23, 'Mr.', 'Et in autem ut ipsa ducimus. Velit temporibus quo dolore nemo provident quos perspiciatis. Optio soluta provident doloribus aut temporibus eos voluptatibus. Culpa esse maiores fugiat et.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Orthotist OR Prosthetist'),
(24, 'Dr.', 'Quis voluptate qui rerum. Nostrum voluptates temporibus suscipit impedit debitis. Rerum qui laudantium officiis qui et rerum quia accusamus. Nisi tenetur inventore accusantium dolor et blanditiis.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Advertising Sales Agent'),
(25, 'Prof.', 'Consectetur velit ut repudiandae velit debitis. Ut ab veniam tempore ullam cupiditate ullam quasi. Natus harum dolores odio aut fugiat molestias nihil.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Multiple Machine Tool Setter'),
(26, 'Miss', 'Ut suscipit omnis aperiam sed tempore. Sit corrupti in vel est nostrum dolorem enim. Qui sed consequatur quae qui. Fugit est nesciunt suscipit consequatur velit ullam quod. Maiores et id reprehenderit nemo animi omnis facilis voluptatem.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Carpenter'),
(27, 'Prof.', 'Quibusdam quo consequatur repellat aut adipisci occaecati placeat. Eum optio accusantium harum quibusdam qui tempore. Voluptatem qui sed velit ea iusto tempora ipsam dolor.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Bindery Worker'),
(28, 'Dr.', 'Ab qui sapiente consequatur doloribus voluptatibus. Aut omnis delectus aut unde sed dolore. Aspernatur commodi non voluptatem.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Foreign Language Teacher'),
(29, 'Mrs.', 'Occaecati est voluptatem quis consequuntur consectetur. Nesciunt possimus numquam magni beatae nostrum. Saepe unde voluptates atque sed atque hic. Aut blanditiis quod vel dolorum cumque.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Manufacturing Sales Representative'),
(30, 'Miss', 'Sed quaerat velit quod iure dolor. Minima nam et aut sed. Consequatur quia vel consectetur omnis mollitia quis nostrum. Aut deleniti itaque assumenda dicta et cumque.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Biological Science Teacher'),
(31, 'Prof.', 'Temporibus illo minus corrupti reiciendis corrupti quia. Voluptatem itaque maiores dolores atque aut. Expedita libero corrupti molestiae ipsam eum ratione maiores.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Food Servers'),
(32, 'Mr.', 'Necessitatibus consequatur eaque voluptas autem. Ut consequatur quo et deserunt beatae vitae qui. Necessitatibus dolores pariatur ad enim et voluptatem. Dolor ex impedit ea.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Private Detective and Investigator'),
(33, 'Mrs.', 'Est unde sint expedita rerum. Debitis qui nulla cupiditate excepturi porro. Fuga aperiam aperiam vero ea.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Housekeeping Supervisor'),
(34, 'Prof.', 'Mollitia quidem modi voluptatum aspernatur velit autem enim nisi. Dicta inventore temporibus ipsum ducimus commodi deleniti eius. Expedita est quia ducimus perferendis.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Foundry Mold and Coremaker'),
(35, 'Ms.', 'Laborum autem culpa itaque expedita doloribus eius. Sed quo magnam illum officia inventore voluptatem. Ullam fuga neque voluptatem voluptas saepe dolore.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Construction'),
(36, 'Mrs.', 'Asperiores iure eius quis eveniet doloribus. Corrupti explicabo tempora omnis est possimus minus qui. Aut quidem reiciendis explicabo iusto velit molestiae.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Ticket Agent'),
(37, 'Prof.', 'Numquam magni voluptatum doloremque iure pariatur praesentium. Enim explicabo non et occaecati ducimus nemo rerum. Corrupti fugit totam ducimus quas accusantium ad provident temporibus.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Pharmacist'),
(38, 'Mrs.', 'Illo adipisci quia voluptatum natus nesciunt nihil eos qui. Vel consectetur ipsum beatae nostrum esse itaque. Ratione qui animi eum voluptatem magnam iusto. Maxime qui temporibus dignissimos beatae sit earum eveniet.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Receptionist and Information Clerk'),
(39, 'Dr.', 'Sit consequuntur magnam voluptatibus. Dignissimos quis repudiandae et aut et. Debitis vel et odit voluptatem dolores eum quia at. Molestiae suscipit corrupti cum sequi voluptas maxime mollitia.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Industrial Production Manager'),
(40, 'Dr.', 'Voluptates repellat voluptatem iusto est qui. Est consectetur est omnis ad debitis et. Autem quaerat vel nemo nihil accusantium. Repellat sint sit praesentium deserunt.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Industrial Machinery Mechanic'),
(41, 'Ms.', 'Velit dolorem sequi non aperiam blanditiis repellat. Animi ducimus qui suscipit autem aut. Sed molestiae qui et nobis. Consequuntur labore et reprehenderit ipsum qui quibusdam.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Computer Software Engineer'),
(42, 'Prof.', 'Velit quia ut dolorum molestias sint ea qui optio. Et aperiam officiis molestiae asperiores id sunt eum qui.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Heating Equipment Operator'),
(43, 'Dr.', 'Quam esse quos et enim perferendis. Perspiciatis illum qui eos quia. Veniam rerum quam dicta quam. Non quaerat fuga quibusdam quod.', '2020-09-16 18:26:53', '2020-09-16 18:26:53', NULL, 'Supervisor of Customer Service'),
(44, 'Dr.', 'Mollitia animi tempore sapiente doloribus sunt. Quam sunt distinctio rerum deserunt. Excepturi veniam natus ut aperiam iusto. Nulla adipisci nemo repudiandae ut aut eveniet dolore et. Enim et ea non beatae et eum ea.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Biomedical Engineer'),
(45, 'Prof.', 'In quaerat exercitationem expedita praesentium consequatur voluptatem voluptatum rerum. Sit nam eaque aliquid consequatur atque cum. Dignissimos deleniti debitis perferendis tempore distinctio.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Respiratory Therapy Technician'),
(46, 'Mr.', 'Qui ipsum maxime sint deleniti assumenda cupiditate. Rem velit accusantium aliquam distinctio. Pariatur tenetur eligendi qui iusto.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Statistical Assistant'),
(47, 'Mr.', 'Eos totam et sed non eos quibusdam nam. Quod expedita corrupti et architecto natus. Voluptatem possimus provident quidem hic laudantium fugiat. Dolor qui quae officiis sunt est cupiditate enim.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Drycleaning Machine Operator'),
(48, 'Ms.', 'Ad sed modi minima officia velit natus quisquam. Magni similique mollitia illum nisi sequi rerum. Vitae eum vero fugiat fugiat et. Autem consectetur voluptatibus quos ea.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Manager of Air Crew'),
(49, 'Mr.', 'Incidunt inventore minima odit omnis. Aut ut sequi beatae magnam eum. Nam minima doloribus non autem similique.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Molding and Casting Worker'),
(50, 'Prof.', 'Saepe fuga ipsum amet dolorum voluptate eaque. Tempora ad non et reiciendis nihil. Sed et consequatur praesentium odio officia minus. Non est dolor totam eos excepturi blanditiis.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Weapons Specialists'),
(51, 'Mrs.', 'Dolorem nam voluptas omnis aut. Sequi et autem ut perferendis. Commodi nemo dolores molestiae illo provident assumenda sequi. Maiores mollitia quam sed nobis.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Annealing Machine Operator'),
(52, 'Prof.', 'Consequatur praesentium voluptatum harum molestiae facilis. Beatae libero consequatur molestiae possimus itaque dolorem rerum vitae. Soluta veritatis hic fugiat quidem nam accusantium ullam. Et dicta exercitationem laborum dolorem nisi temporibus id.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Social Media Marketing Manager'),
(53, 'Miss', 'Cupiditate itaque debitis similique blanditiis amet voluptatum sint. Est in id optio aut rem. Doloremque dolorum eligendi deserunt. Sed sit et et ipsum.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Night Security Guard'),
(54, 'Dr.', 'Occaecati eos molestias doloremque officiis porro voluptates quod. Ea voluptas beatae nam suscipit. Aliquam dolor provident et placeat nobis qui.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Mechanical Inspector'),
(55, 'Prof.', 'Sapiente fuga architecto omnis quibusdam laboriosam. Ea expedita eaque dolores necessitatibus nam exercitationem voluptatibus et. Ex commodi et in recusandae cum autem.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Healthcare Practitioner'),
(56, 'Dr.', 'Soluta doloribus autem deserunt voluptas quia natus. Aut voluptas aut officia debitis ipsa repudiandae veniam et. Ad voluptas nostrum porro quae est.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Network Admin OR Computer Systems Administrator'),
(57, 'Dr.', 'Pariatur repudiandae sed non dolorum neque molestiae voluptatem. Vel tenetur eaque esse. Fugit quasi placeat vel voluptate quisquam debitis. Perferendis sunt autem aut libero assumenda sunt.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Painting Machine Operator'),
(58, 'Mrs.', 'Labore ut ut veritatis praesentium repellendus nam. Ipsam repellat ex ea doloribus. Laboriosam id tempora eum suscipit rerum.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Command Control Center Officer'),
(59, 'Prof.', 'Blanditiis voluptas eum quas omnis sint. Quis error sint odio numquam qui. Rerum rerum totam voluptatem veniam itaque dolores.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Structural Metal Fabricator'),
(60, 'Ms.', 'Voluptas deleniti dolores quibusdam vitae. Enim est est nulla voluptas autem. Impedit itaque commodi autem laborum. Ut odio quas hic blanditiis voluptate.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Hand Trimmer'),
(61, 'Ms.', 'Aut nostrum harum magni aut autem. Distinctio quaerat qui enim explicabo a magnam eum. Dolorem labore repellendus provident libero vel voluptas. Odio accusamus ut magnam molestiae.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Molding Machine Operator'),
(62, 'Miss', 'Quia numquam et rerum qui. Repudiandae consequatur et eos veniam tempore eligendi quis itaque. Eos reiciendis pariatur eum dolor.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Office and Administrative Support Worker'),
(63, 'Mrs.', 'Sunt fugit recusandae aperiam sunt vitae vero. Ut architecto similique totam consequatur. Magnam recusandae commodi facilis veniam. Et quia nemo dolor mollitia quia.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Photoengraving Machine Operator'),
(64, 'Ms.', 'Nemo consequatur optio eius hic doloremque ipsum. Eius ad delectus facilis qui facere atque quos. Dolores odit quasi consequuntur sequi ipsam voluptatem in. Ullam et inventore illum quam dignissimos error et.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Director Religious Activities'),
(65, 'Prof.', 'Veritatis unde quia sit tempore. Maiores nobis iusto commodi alias recusandae aspernatur aut ipsa.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Waiter'),
(66, 'Prof.', 'Laborum nulla sunt iusto eum ducimus nulla. Odio ex at quasi aut soluta. Doloribus molestias sequi nesciunt occaecati. Rerum ipsam corporis ut adipisci praesentium nesciunt.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Locomotive Engineer'),
(67, 'Mrs.', 'Ut sed aliquam quam qui tempore maxime. Sit dolor magni ipsam. Architecto impedit necessitatibus et aut perspiciatis.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Social Media Marketing Manager'),
(68, 'Mr.', 'Quaerat et quidem temporibus incidunt soluta. Nihil voluptatem voluptate sit repudiandae non voluptas suscipit. Autem dolor ut in est fuga. Molestiae consequatur exercitationem error enim sed sapiente voluptas expedita.', '2020-09-16 18:26:54', '2020-09-16 18:26:54', NULL, 'Cardiovascular Technologist'),
(69, 'Prof.', 'Quos ipsa adipisci voluptatem reprehenderit sit in beatae consequatur. Odit laboriosam aut tempore. Reprehenderit incidunt a aut quis et voluptas.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Licensing Examiner and Inspector'),
(70, 'Mr.', 'Architecto temporibus velit voluptatem fugiat quo illo non. Possimus veniam repellat officiis cum illo debitis totam. Et aut voluptatum et et commodi et. Pariatur qui omnis error maiores quas est et quaerat.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Social Work Teacher'),
(71, 'Dr.', 'Nemo expedita qui dolor itaque. Et explicabo quisquam tenetur sed blanditiis quos.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Travel Clerk'),
(72, 'Prof.', 'Nihil ad possimus officia nulla quia temporibus. Laboriosam voluptatibus quibusdam vel molestiae. Doloremque voluptatum magnam sequi molestiae.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Parts Salesperson'),
(73, 'Dr.', 'Ipsam et dolor quam molestiae. Est ipsum quia explicabo qui eum. Temporibus non optio vitae quibusdam vero repudiandae laboriosam.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Animal Trainer'),
(74, 'Dr.', 'Consequatur architecto sit cum et doloremque. Et fugit incidunt voluptatem voluptatem quae voluptas. Vel ipsam quas corrupti reiciendis ut. Nam maxime praesentium laborum ea iste adipisci harum.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Machine Feeder'),
(75, 'Prof.', 'Ullam voluptas et consectetur reiciendis atque autem quia. Mollitia autem veritatis voluptate veritatis accusantium.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Medical Transcriptionist'),
(76, 'Dr.', 'Assumenda deleniti qui qui voluptatem impedit. Consequatur et enim sint rem sunt ratione sed. Doloribus eveniet consequatur corporis. Voluptas quo et velit nisi molestiae et.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Food Servers'),
(77, 'Ms.', 'Ut neque magni officiis doloribus consequuntur placeat. Et aut soluta laboriosam et maxime.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Director Of Marketing'),
(78, 'Ms.', 'Assumenda fuga quo quaerat non officiis labore consequuntur itaque. Voluptatem odit delectus repudiandae commodi doloribus eius unde. Quibusdam quidem ducimus rerum sit. Sed cumque sint eius corrupti in. Rerum quia quod est est adipisci aut nam et.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Forensic Investigator'),
(79, 'Miss', 'Hic distinctio odio velit. Sequi nihil iusto qui fugit quia dolorem eos.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Food Servers'),
(80, 'Mr.', 'Quia dolorum libero quam minus illo harum nesciunt. Molestias dolorem ut quam numquam veritatis. Voluptas temporibus atque qui itaque. Eligendi nesciunt molestiae eum excepturi dolorum iste minus.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Sys Admin'),
(81, 'Mr.', 'Accusantium debitis eum voluptas eos. Aliquam possimus velit quis ut culpa tenetur. Voluptatibus delectus cupiditate voluptatem harum. Harum repudiandae et veritatis voluptates.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Construction Driller'),
(82, 'Dr.', 'Numquam hic doloribus autem recusandae illo optio. Sunt vero dolores omnis omnis qui consectetur. Minus qui quaerat sint molestiae aut modi.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Coil Winders'),
(83, 'Miss', 'Cumque incidunt in voluptatem iusto rerum saepe. Repellat officiis consectetur dolorem excepturi. Est quia velit qui qui ullam et qui.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Numerical Control Machine Tool Operator'),
(84, 'Prof.', 'Explicabo et accusantium eius qui ab culpa. Enim voluptas voluptas quo modi qui. Nulla repudiandae aliquid quaerat molestiae odio cumque.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Social and Human Service Assistant'),
(85, 'Mr.', 'Beatae ut quae qui id nostrum sed vel earum. Sapiente perspiciatis sed vel enim delectus dolores laborum. Et facere libero corporis non assumenda. Quasi eum blanditiis et impedit atque quia totam.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Farm and Home Management Advisor'),
(86, 'Dr.', 'Ducimus similique nam aut asperiores praesentium nisi. Placeat dolor facilis expedita voluptatibus placeat atque cum. Et nihil fuga officia distinctio sint aliquid temporibus.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Interpreter OR Translator'),
(87, 'Dr.', 'Sed voluptas eaque blanditiis nulla qui. Consequatur enim quod officiis dolores. Doloribus vel fugit qui harum rerum.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Police Detective'),
(88, 'Dr.', 'Et iure vel et aut temporibus ipsam. Quis fugit quae beatae. Enim quam a molestiae quasi ab. Et doloremque alias voluptatibus eligendi ut.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Environmental Scientist'),
(89, 'Miss', 'Sit distinctio ea quo. Aliquam a omnis sequi magni doloribus. Velit veritatis quasi iste quos nostrum. Aliquam animi repellendus quo.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Cost Estimator'),
(90, 'Ms.', 'Ea quae sequi ut veniam possimus. Aut nostrum iure nemo quis voluptatum nobis impedit. Doloribus qui et non impedit voluptate illum soluta.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Vice President Of Human Resources'),
(91, 'Mr.', 'Suscipit nemo quod labore eos. Alias nulla odio ipsa autem corrupti nihil. Eum blanditiis cum et ipsum. Et recusandae eveniet et non consequuntur qui temporibus.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Desktop Publisher'),
(92, 'Mr.', 'Voluptatem culpa in adipisci dolorem nostrum. Esse eos commodi enim aspernatur aut maiores. Esse porro possimus in. Vel dolorem maxime sint sint totam.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Woodworker'),
(93, 'Prof.', 'Dolores et nam nihil. Voluptas et ea vero ipsa quia. Dicta repellendus eos culpa ut recusandae perspiciatis.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Laundry OR Dry-Cleaning Worker'),
(94, 'Mr.', 'Quis ipsa veniam quia voluptatem. Accusamus vel aut recusandae perferendis quidem. Necessitatibus dolorem sit aspernatur quisquam. Sit modi eum labore ut quaerat.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Textile Machine Operator'),
(95, 'Mr.', 'Cupiditate eveniet dolor provident enim ut et eius. Repudiandae minus deserunt sit similique ratione dolor ea dignissimos. Quis dicta aliquid dolor perspiciatis eligendi dolor.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Patternmaker'),
(96, 'Miss', 'Et recusandae quis minus nihil porro ut molestiae sit. Occaecati ratione debitis nostrum accusamus facilis dolor. Est quos consequatur ut eum.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Decorator'),
(97, 'Mr.', 'Sunt nam nostrum tempora illo aut numquam minus. Consequuntur ex nihil exercitationem.', '2020-09-16 18:26:55', '2020-09-16 18:26:55', NULL, 'Respiratory Therapist'),
(98, 'Dr.', 'Fugit exercitationem deserunt ullam delectus cupiditate. Repellendus itaque dolorem possimus qui. Quasi veniam est enim veniam et error. Facilis consectetur minus quae nihil iure.', '2020-09-16 18:26:56', '2020-09-16 18:26:56', NULL, 'Special Forces Officer'),
(99, 'Prof.', 'Error quo necessitatibus aliquam dolor labore error mollitia. Molestias enim quae amet harum voluptatem impedit et. Maxime possimus blanditiis sit praesentium autem officia non repudiandae. Necessitatibus pariatur necessitatibus velit omnis molestiae ipsa.', '2020-09-16 18:26:56', '2020-09-16 18:26:56', NULL, 'Teller'),
(100, 'Dr.', 'Eius officia qui odit ullam pariatur facere. Eaque sint unde beatae et quia maxime. Sapiente provident corporis est vitae ut quos.', '2020-09-16 18:26:56', '2020-09-16 18:26:56', NULL, 'Maintenance Worker'),
(101, 'Ms.', 'Repudiandae nihil illo laboriosam optio sint ab est. Ut accusamus ab aliquam maiores nulla dolor quos. Aut dicta repellendus porro quia ipsa aperiam.', '2020-09-16 18:26:56', '2020-09-16 18:26:56', NULL, 'Stock Broker'),
(102, 'Prof.', 'Et voluptatem suscipit quisquam commodi assumenda laborum. Qui sit praesentium ut. Rerum dolore non ratione placeat. Minima ullam et nihil tempora voluptas eligendi nemo.', '2020-09-16 18:26:56', '2020-09-16 18:26:56', NULL, 'Extruding Machine Operator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2020_09_17_002211_create_blog_table', 1),
(6, '2020_09_17_003447_add_category_in_blog_table', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
